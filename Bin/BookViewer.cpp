//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                  ---  Household Accounts Core.  ---                  **
**                                                                      **
**          Copyright (C), 2017-2017, Takahiro Itou                     **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      ビューワープログラム。
**
**      @file       Bin/BookViewer.cpp
**/

#include    "HouseholdAccounts/Common/HouseholdAccountsSettings.h"

#include    <iostream>

using   namespace   HOUSEHOLD_ACCOUNTS_NAMESPACE;

int  main(int argc, char * argv[])
{
    return ( 0 );
}
